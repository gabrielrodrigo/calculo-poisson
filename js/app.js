function Controle() {
    var botaoCalcular = document.getElementById('btnCalcular');
    var botaoNovo = document.getElementById('btnNovo');

    botaoCalcular.onclick = function() {
        var valorMedia = document.getElementById('media').value;
        var valorX = document.getElementById('valorX').value;
        var p = document.getElementById('Resultado');

        if (valorMedia == "") {
            alert("O campo MÉDIA não pode ficar vazio.");
            valorMedia.focus();
        } else if (valorX == "") {
            alert("O campo X não pode ficar vazio.");
            valorX.focus();
        } else {
            var fatorial;
            var resultadoFinal;

            fatorial = CalcularFatorial(valorX);
            resultadoFinal = CalcularPoisson(valorX, valorMedia, fatorial);

            p.insertAdjacentHTML('beforeend', (resultadoFinal * 100).toPrecision(3) + " %");
        }
    };

    botaoNovo.onclick = function() {
        location.reload();
    };
}

function CalcularFatorial(ValorX) {
    if (ValorX === 0) {
        return 1;
    } else {
        return ValorX * CalcularFatorial(ValorX - 1);
    }
}

function CalcularPoisson(X, Media, Fat) {
    var e = 2.71828;
    var resultadoPrincipal;
    var resultado1;
    var resultado2;

    resultado1 = Math.pow(Media, X);
    resultado2 = Math.pow(e, -Media);

    resultado1 = resultado1 * resultado2;
    resultadoPrincipal = resultado1 / Fat;

    return resultadoPrincipal;
}

Controle();